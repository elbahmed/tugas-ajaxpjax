<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            'created_at',
            'updated_at',
            //'created_by',
            //'updated_by',

            [                
                'class' => 'yii\grid\ActionColumn',

                'header' => '<a>Action</a>',

                'contentOptions' => ['style' => 'width: 6%'],

            	'template' => '{view} {update} {delete}',

            	'buttons' => [
                
            		'update' => function ($url, $model, $key) {
                    
            			return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->__get('id')]);
                        
            		},
                
            		'view' => function ($url, $model, $key) {
                    
            				return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->__get('id')]);
                    
                    },
                    
                    'delete' => function($url, $model, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                                    'class' => 'delete',
                                    'id' => $model->__get('id'),
                                    'data-target' => '#myModal',
                                    'data-toggle' => 'modal'
                                ]);
                    },
                
            	],
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
